import { Component, OnInit, ViewChild } from '@angular/core';
import { MemberService } from '../services/member.service';
import { AlertifyService } from '../services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from './../services/login.service';
import { Note } from '../models/Note';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  @ViewChild('f') addForm: NgForm;
  notes: Note[];
  parmmaster: any;
  model: any = {};
  nametoken: any;
  shownote: boolean;
  editing = false;
  note: Note;
  slevel: number;
  value;
  idAttr;
  name: string;

  constructor(
    private memberService: MemberService,
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private loginService: LoginService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loginService.currentlogin.subscribe(slevel => (this.slevel = slevel));
    this.loginService.currentName.subscribe(name => (this.name = name));
    console.log(this.slevel);
    this.shownote = false;
    this.loadNotes();
    this.parmmaster = this.route.snapshot.params['master'];
  }

  loadNotes() {
    this.memberService.getNotes(this.route.snapshot.params['master']).subscribe(
      (notes: Note[]) => {
        this.notes = notes;
      },
      error => {
        this.alertify.error('did not work');
      }
    );
  }

  Submit() {
    this.model.master_id = this.parmmaster;
    this.model.created_by = this.name;
    this.memberService.addNote(this.model).subscribe(
      () => {
        console.log(this.model);
        this.alertify.success('Note has been added.');
        this.loadNotes();
      },
      error => {
        this.alertify.error('The Note could not be saved.');
      }
    );
  }
  showform() {
    this.shownote = !this.shownote;
    console.log(this.shownote);
  }

  showedit(event) {
    this.editing = true;
    this.idAttr = event.srcElement.attributes.id;
    this.value = this.idAttr.nodeValue;
    console.log(this.value);
    this.memberService.getOneNote(this.value).subscribe(
      (note: Note) => {
        this.note = note;
        console.log(note);
      },
      error => {
        this.alertify.error('did not work');
      }
    );
  }

  onDelete(id: string) {
    this.alertify.confirm('Are you sure you want to delete this note?', () => {
      this.memberService.deleteNote(id).subscribe(
        () => {
          this.alertify.success('Note Deleted');
          this.loadNotes();
        },
        error => {
          this.alertify.error('Failed to delete...');
        }
      );
    });
  }
}
