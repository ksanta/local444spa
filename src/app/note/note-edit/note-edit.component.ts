import { Component, OnInit, ViewChild } from '@angular/core';
import { Note } from '../../models/Note';
import { MemberService } from '../../services/member.service';
import { AlertifyService } from '../../services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-note-edit',
  templateUrl: './note-edit.component.html',
  styleUrls: ['./note-edit.component.css']
})
export class NoteEditComponent implements OnInit {
  note: Note;
  isLoading = false;
  @ViewChild('f') editForm: NgForm;
  masterload: string;

  constructor(
    private memberService: MemberService,
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.loadNote();
  }

  loadNote() {
    this.isLoading = true;
    this.memberService.getOneNote(this.route.snapshot.params['id']).subscribe(
      (note: Note) => {
        this.note = note;
        this.masterload = note.master_id;
        this.isLoading = false;
        this.alertify.success('note loaded!');
      },
      error => {
        this.alertify.error('there has been an error');
        this.isLoading = false;
      }
    );
  }

  updateNote() {
    this.memberService
      .updateNote(this.route.snapshot.params['id'], this.note)
      .subscribe(
        next => {
          this.alertify.success('profile updated successfully.');
          this.loadNote();
        },
        error => {
          this.alertify.error('profile could not be saved.');
        }
      );
  }
}
