import { Component, OnInit, ViewChild } from '@angular/core';
import { MemberService } from '../../services/member.service';
import { AlertifyService } from '../../services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { Workplace } from '../../models/Workplace';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-edit-company',
  templateUrl: './edit-company.component.html',
  styleUrls: ['./edit-company.component.css']
})
export class EditCompanyComponent implements OnInit {
  workplace: Workplace;
  @ViewChild('f') editcompany: NgForm;



  constructor(
    private memberService: MemberService,
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.getOneCompany();
  }

  getOneCompany() {
    this.memberService.getOneCompany(this.route.snapshot.params['id']).subscribe(
      (workplace: Workplace) => {
        this.workplace = workplace;
        this.alertify.success('company loaded!');
      },
      error => {
        this.alertify.error('there has been an error');
      }
    );
  }

  updateCompany() {
    this.memberService
      .updateCompany(this.route.snapshot.params['id'], this.workplace)
      .subscribe(
        next => {
          this.alertify.success('company updated successfully.');
          this.router.navigateByUrl('/view-companies');
        },
        error => {
          this.alertify.error('company could not be saved.');
        }
      );
  }
}
