import { Component, OnInit, ViewChild } from '@angular/core';
import { MemberService } from '../../services/member.service';
import { AlertifyService } from '../../services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from './../../services/login.service';
import { Workplace } from 'src/app/models/Workplace';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-view-companies',
  templateUrl: './view-companies.component.html',
  styleUrls: ['./view-companies.component.css']
})
export class ViewCompaniesComponent implements OnInit {
  @ViewChild('f') addCompany: NgForm;
  slevel: number;
  name: string;
  workplaces: Workplace[];
  shownote: boolean;
  editing = false;
  model: any = {};

  constructor(private memberService: MemberService,
              private alertify: AlertifyService,
              private route: ActivatedRoute,
              private loginService: LoginService,
              private router: Router) { }

  ngOnInit() {
    this.loginService.currentlogin.subscribe(slevel => (this.slevel = slevel));
    this.loginService.currentName.subscribe(name => (this.name = name));
    this.loadCompanyList();
  }

  loadCompanyList() {
    this.memberService.getCompanys().subscribe(
      (workplaces: Workplace[]) => {
        this.workplaces = workplaces;
        console.log(this.workplaces);
      },
      error => {
        this.alertify.error('did not work');
      }
    );
  }

  showform() {
    this.shownote = !this.shownote;
    console.log(this.shownote);
  }

  Submit() {
  this.memberService.addCompany(this.model).subscribe(
    () => {
      console.log(this.model);
      this.alertify.success('Company has been added.');
      location.reload();
    },
    error => {
      this.alertify.error('The Company could not be saved.');
    }
  );
}

onDelete(id: string) {
  this.alertify.confirm('Are you sure you want to delete this file?', () => {
    this.memberService.deleteCompany(id).subscribe(
      () => {
        this.alertify.success('File Deleted');
        this.loadCompanyList();
      },
      error => {
        this.alertify.error('Failed to delete...');
      }
    );
  });
}

backToLeaves() {
  this.router.navigateByUrl('/leaves');
}
}

