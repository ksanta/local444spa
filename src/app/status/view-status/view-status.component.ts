import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Ustatus } from '../../models/Ustatus';
import { MemberService } from '../../services/member.service';
import { AlertifyService } from '../../services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../../services/login.service';


@Component({
  selector: 'app-view-status',
  templateUrl: './view-status.component.html',
  styleUrls: ['./view-status.component.css']
})
export class ViewStatusComponent implements OnInit {
  @ViewChild('f') addStatus: NgForm;
  slevel: number;
  name: string;
  status: Ustatus[];
  shownote: boolean;
  editing = false;
  model: any = {};

  constructor(private memberService: MemberService,
              private alertify: AlertifyService,
              private route: ActivatedRoute,
              private loginService: LoginService,
              private router: Router) { }

  ngOnInit() {
    this.loginService.currentlogin.subscribe(slevel => (this.slevel = slevel));
    this.loginService.currentName.subscribe(name => (this.name = name));
    this.loadStatusList();
  }

  loadStatusList() {
    this.memberService.getStatus().subscribe(
      (status: Ustatus[]) => {
        this.status = status;
        console.log(this.status);
      },
      error => {
        this.alertify.error('did not work');
      }
    );
  }

  showform() {
    this.shownote = !this.shownote;
    console.log(this.shownote);
  }

  Submit() {
  this.memberService.addStatus(this.model).subscribe(
    () => {
      console.log(this.model);
      this.alertify.success('Company has been added.');
      location.reload();
    },
    error => {
      this.alertify.error('The Company could not be saved.');
    }
  );
}

onDelete(id: string) {
  this.alertify.confirm('Are you sure you want to delete this file?', () => {
    this.memberService.deleteStatus(id).subscribe(
      () => {
        this.alertify.success('File Deleted');
        this.loadStatusList();
      },
      error => {
        this.alertify.error('Failed to delete...');
      }
    );
  });
}
  }

