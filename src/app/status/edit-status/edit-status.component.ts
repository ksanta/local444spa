import { Component, OnInit, ViewChild } from '@angular/core';
import { MemberService } from '../../services/member.service';
import { AlertifyService } from '../../services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { Ustatus } from '../../models/Ustatus';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-edit-status',
  templateUrl: './edit-status.component.html',
  styleUrls: ['./edit-status.component.css']
})
export class EditStatusComponent implements OnInit {
  status: Ustatus;
  @ViewChild('f') editstatus: NgForm;



  constructor(
    private memberService: MemberService,
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.getOneStatus();
  }

  getOneStatus() {
    this.memberService.getOneStatus(this.route.snapshot.params['id']).subscribe(
      (status: Ustatus) => {
        this.status = status;
        this.alertify.success('status loaded!');
      },
      error => {
        this.alertify.error('there has been an error');
      }
    );
  }

  updateStatus() {
    this.memberService
      .updateStatus(this.route.snapshot.params['id'], this.status)
      .subscribe(
        next => {
          this.alertify.success('status updated successfully.');
          this.router.navigateByUrl('view-status');
        },
        error => {
          this.alertify.error('status could not be saved.');
        }
      );
  }
}
