export interface Note {
  id: number;
  master_id: string;
  info: string;
  created_by: string;
  created_at: Date;
}
