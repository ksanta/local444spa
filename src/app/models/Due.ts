export interface Due {
  id: number;
  master_id: string;
  year: string;
  type: string;
  start_date: Date;
  end_date: Date;
  amount: number;
}
