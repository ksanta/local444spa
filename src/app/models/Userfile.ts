export interface Userfile {
  id: number;
  master_id: string;
  info: string;
  file: string;
  created_by: string;
  created_at: Date;
}
