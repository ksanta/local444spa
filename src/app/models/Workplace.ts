export interface Workplace {
  id: number;
  workplace: string;
  emails: string;
}
