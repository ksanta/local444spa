import { Lmember } from './Lmember';

export interface Leave {
  id: number;
  company: string;
  type: string;
  notes: string;
  sub_note: string;
  notation: string;
  typer: string;
  created_at: Date;
  updated_at: Date;
  subleaves: Array<Lmember>;
}
