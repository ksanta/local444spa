export class Lmember {
  constructor(
    public leave_no: string,
    public master: string,
    public first: string,
    public last: string,
    public dept: string,
    public notation: string,
  ) {}
}
