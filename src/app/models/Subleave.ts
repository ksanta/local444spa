export interface Subleave {
  id: number;
  leave_no: number;
  master: string;
  first: string;
  dept: string;
  notation: string;
  created_at: Date;
  updated_at: Date;
}
