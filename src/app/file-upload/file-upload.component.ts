import { Router } from '@angular/router';
import { AlertifyService } from './../services/alertify.service';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Headers, Http, URLSearchParams } from '@angular/http';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  form: FormGroup;
  loading = false;
  @Input() name;
  @Input() parmmaster;

  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(
    private fb: FormBuilder,
    private http: Http,
    private alertify: AlertifyService,
    private router: Router
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      master_id: [this.parmmaster],
      info: ['', Validators.required],
      created_by: [this.name, Validators.required],
      ufile: null
    });
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.form.get('ufile').setValue(file);
    }
  }

  private prepareSave(): any {
    let input = new FormData();
    input.append('master_id', this.form.get('master_id').value);
    input.append('info', this.form.get('info').value);
    input.append('created_by', this.form.get('created_by').value);
    input.append('ufile', this.form.get('ufile').value);
    return input;
  }

  onSubmit() {
    const formModel = this.prepareSave();
    this.loading = true;
    this.http
      .post('http://localhost/laravelapi/public/api/files', formModel)
      .subscribe(
        res => {
          this.alertify.success('File Added');
          this.loading = false;
          this.router.navigateByUrl('member-view/' + this.parmmaster);
        },
        error => {
          this.alertify.error('The file could not be saved.');
        }
      );
  }

  clearFile() {
    this.form.get('ufile').setValue(null);
    this.fileInput.nativeElement.value = '';
  }
}
