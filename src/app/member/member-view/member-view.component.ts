import { Component, OnInit } from '@angular/core';
import { Member } from '../../models/Member';
import { ActivatedRoute } from '@angular/router';
import { AlertifyService } from '../../services/alertify.service';
import { MemberService } from '../../services/member.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-member-view',
  templateUrl: './member-view.component.html',
  styleUrls: ['./member-view.component.css']
})
export class MemberViewComponent implements OnInit {
  member: Member;
  isLoading = false;
  slevel: number;

  constructor(
    private memberService: MemberService,
    private route: ActivatedRoute,
    private alertify: AlertifyService,
    private loginService: LoginService
  ) {}

  ngOnInit() {
    this.loginService.currentlogin.subscribe(slevel => (this.slevel = slevel));
    console.log(this.slevel);
    this.loadMember();
  }

  loadMember() {
    this.isLoading = true;
    this.memberService
      .getMember(this.route.snapshot.params['master'])
      .subscribe(
        (member: Member) => {
          this.member = member;
          console.log(this.member);
          this.isLoading = false;
          this.alertify.success('member loaded!');
        },
        error => {
          this.alertify.error('there has been an error');
          this.isLoading = false;
        }
      );
  }
}
