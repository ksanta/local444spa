import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Member } from '../../models/Member';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from '../../services/alertify.service';
import { Workplace } from '../../models/Workplace';
import { Ustatus } from '../../models/Ustatus';
import { Funeralhome } from '../../models/Funeralhome';
import { MemberService } from '../../services/member.service';
import * as moment from 'moment';
import { NgForm, FormControl } from '@angular/forms';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-add.component.html',
  styleUrls: ['./member-add.component.css']
})
export class MemberAddComponent implements OnInit {
  @ViewChild('f') addForm: NgForm;
  member: Member;
  workplaces: Workplace[];
  ustatus: Ustatus[];
  funeralhomes: Funeralhome[];
  model: any = {};
  isLoading = false;
  routeMaster = this.route.snapshot.params.master;
  mask: any[] = [
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';
  sinPattern = '^\d{3}(-|){1}\d{3}(-|){1}\d{3}$';
  hireDate;
  initiationDate;
  withdrawDate;
  statusDate;
  watchDate;
  deathDate;
  mfDate;
  spouseDeath;
  spouseFuneral;

  constructor(
    private memberService: MemberService,
    private route: ActivatedRoute,
    private alertify: AlertifyService,
    private router: Router,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.loadCompanys();
    this.loadUstatus();
    this.loadFuneralHomes();
  }

  loadCompanys() {
    this.memberService.getCompanys().subscribe(
      (workplaces: Workplace[]) => {
        this.workplaces = workplaces;
      },
      error => {
        this.alertify.error('could not pull the list of companies.');
      }
    );
  }

  loadUstatus() {
    this.memberService.getStatus().subscribe(
      (ustatus: Ustatus[]) => {
        this.ustatus = ustatus;
        console.log(this.ustatus);
      },
      error => {
        this.alertify.error('could not pull the status list.');
      }
    );
  }

  loadFuneralHomes() {
    this.memberService.getFhomes().subscribe(
      (funeralhomes: Funeralhome[]) => {
        this.funeralhomes = funeralhomes;
      },
      error => {
        this.alertify.error('could not pull the Funeral Homes.');
      }
    );
  }

  addMember() {
    this.memberService.addMember(this.model).subscribe(
      () => {
        this.alertify.success('Member has been added.');
        this.router.navigateByUrl('/member-edit/' + this.model.master);
      },
      error => {
        this.alertify.error('The new Member could not be saved.');
      }
    );
  }
}
