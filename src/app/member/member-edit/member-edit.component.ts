import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Member } from '../../models/Member';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from '../../services/alertify.service';
import { Workplace } from '../../models/Workplace';
import { Ustatus } from '../../models/Ustatus';
import { Funeralhome } from '../../models/Funeralhome';
import { MemberService } from '../../services/member.service';
import * as moment from 'moment';
import { NgForm, FormControl } from '@angular/forms';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {
  @ViewChild('f') editForm: NgForm;
  member: Member;
  workplaces: Workplace[];
  ustatus: Ustatus[];
  funeralhomes: Funeralhome[];
  model: any = {};
  isLoading = false;
  routeMaster = this.route.snapshot.params.master;
  mask: any[] = [
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';
  sinPattern: any[] = [/\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/];
  hireDate;
  initiationDate;
  withdrawDate;
  statusDate;
  watchDate;
  deathDate;
  mfDate;
  spouseDeath;
  spouseFuneral;
  vdob;
  retDate;

  constructor(
    private memberService: MemberService,
    private route: ActivatedRoute,
    private alertify: AlertifyService,
    private router: Router,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.loadMember();
    this.loadCompanys();
    this.loadUstatus();
    this.loadFuneralHomes();
  }
  loadMember() {
    this.isLoading = true;
    this.memberService.getMember(this.route.snapshot.params.master).subscribe(
      (member: Member) => {
        this.member = member;
        if (this.member.hire_date) {
          this.hireDate = new FormControl(new Date(this.member.hire_date));
        } else {
          this.hireDate = new Date(null);
        }
        if (this.member.initiation) {
          this.initiationDate = new FormControl(
            new Date(this.member.hire_date)
          );
        } else {
          this.initiationDate = new Date(null);
        }
        if (this.member.withdraw) {
          this.withdrawDate = new FormControl(new Date(this.member.withdraw));
        } else {
          this.withdrawDate = new Date(null);
        }
        if (this.member.status_date) {
          this.statusDate = new FormControl(new Date(this.member.status_date));
        } else {
          this.statusDate = new Date(null);
        }
        if (this.member.watch) {
          this.watchDate = new FormControl(new Date(this.member.watch));
        } else {
          this.watchDate = new Date(null);
        }
        if (this.member.member_death_date) {
          this.deathDate = new FormControl(
            new Date(this.member.member_death_date)
          );
        } else {
          this.deathDate = new Date(null);
        }
        if (this.member.member_funeral_date) {
          this.mfDate = new FormControl(
            new Date(this.member.member_funeral_date)
          );
        } else {
          this.mfDate = new Date(null);
        }
        if (this.member.spouse_death_date) {
          this.spouseDeath = new FormControl(
            new Date(this.member.spouse_death_date)
          );
        } else {
          this.spouseDeath = new Date(null);
        }
        if (this.member.spouse_funeral_date) {
          this.spouseFuneral = new FormControl(
            new Date(this.member.spouse_funeral_date)
          );
        } else {
          this.spouseFuneral = new Date(null);
        }
        if (this.member.dob) {
          this.vdob = new FormControl(
            new Date(this.member.dob)
          );
        } else {
          this.vdob = new Date(null);
        }
        if (this.member.retirement_date) {
          this.retDate = new FormControl(
            new Date(this.member.retirement_date)
          );
        } else {
          this.retDate = new Date(null);
        }
        this.alertify.success('Edit member screen loaded');
        this.isLoading = false;
      },
      error => {
        this.alertify.error('Cannot Retrieve Member Information');
        this.isLoading = false;
      }
    );
  }

  loadCompanys() {
    this.memberService.getCompanys().subscribe(
      (workplaces: Workplace[]) => {
        this.workplaces = workplaces;
      },
      error => {
        this.alertify.error('could not pull the list of companies.');
      }
    );
  }

  loadUstatus() {
    this.memberService.getStatus().subscribe(
      (ustatus: Ustatus[]) => {
        this.ustatus = ustatus;
        console.log(this.ustatus);
      },
      error => {
        this.alertify.error('could not pull the status list.');
      }
    );
  }

  loadFuneralHomes() {
    this.memberService.getFhomes().subscribe(
      (funeralhomes: Funeralhome[]) => {
        this.funeralhomes = funeralhomes;
      },
      error => {
        this.alertify.error('could not pull the Funeral Homes.');
      }
    );
  }

  updateMember() {
    this.editForm.form.patchValue({
      hire_date: new Date(this.hireDate.value),
      initiation: new Date(this.initiationDate.value),
      withdraw: new Date(this.withdrawDate.value),
      status_date: new Date(this.statusDate.value),
      watch: new Date(this.watchDate.value),
      member_death_date: new Date(this.deathDate.value),
      member_funeral_date: new Date(this.mfDate.value),
      spouse_death_date: new Date(this.spouseDeath.value),
      spouse_funeral_date: new Date(this.spouseFuneral.value),
      dob: new Date(this.vdob.value),
      retirement_date: new Date(this.retDate.value)
    }),
      this.memberService
        .updateMember(this.route.snapshot.params['master'], this.member)
        .subscribe(
          next => {
            this.alertify.success('profile updated successfully.');
          },
          error => {
            this.alertify.error('profile could not be saved.');
          }
        );
  }
}
