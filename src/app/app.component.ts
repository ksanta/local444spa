import { NewLeaveComponent } from './leave/leaves/new-leave/new-leave.component';
import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Local 444';
  showHeader = true;
  router;

  constructor(private _router: Router) {
    this.router = _router;
  }

  modifyHeader() {
    // This method is called many times
    console.log(this.router.url, 'working'); // This prints a loot of routes on console
    if (this.router.url === '/leave-one/1') {
      this.showHeader = false;
      console.log(this.showHeader);
    } else {
      this.showHeader = true;
      console.log(this.showHeader);
    }
  }
}
