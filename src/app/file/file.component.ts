import { LoginService } from './../services/login.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Userfile } from '../models/Userfile';
import { MemberService } from '../services/member.service';
import { AlertifyService } from '../services/alertify.service';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit {
  @ViewChild('f') addFile: NgForm;
  files: Userfile[];
  parmmaster: any;
  model: any = {};
  showFile: boolean;
  value;
  idAttr;
  slevel: number;
  name: string;

  constructor(
    private memberService: MemberService,
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private loginService: LoginService
  ) {}

  ngOnInit() {
    this.loginService.currentlogin.subscribe(slevel => (this.slevel = slevel));
    this.loginService.currentName.subscribe(name => (this.name = name));
    this.showFile = false;
    this.loadFiles();
    this.parmmaster = this.route.snapshot.params['master'];
  }

  loadFiles() {
    this.memberService.getFiles(this.route.snapshot.params['master']).subscribe(
      (files: Userfile[]) => {
        this.files = files;
        console.log(this.files);
      },
      error => {
        this.alertify.error('did not work');
      }
    );
  }

  showform() {
    this.showFile = !this.showFile;
  }

  onDelete(id: string) {
    this.alertify.confirm('Are you sure you want to delete this file?', () => {
      this.memberService.deleteFile(id).subscribe(
        () => {
          this.alertify.success('File Deleted');
          this.loadFiles();
        },
        error => {
          this.alertify.error('Failed to delete...');
        }
      );
    });
  }
}
