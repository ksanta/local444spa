import { Component, OnInit, Inject } from '@angular/core';
import { AlertifyService } from './../services/alertify.service';
import { MemberService } from './../services/member.service';
import { Member } from '../models/Member';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  keyword = '';
  members = [];
  isLoading = false;
  p = 1;
  recount = 1;
  slevel: number;

  constructor(
    private memberService: MemberService,
    private alertify: AlertifyService,
    private loginService: LoginService,
    private route: Router
  ) {}

  ngOnInit() {
    this.loginService.currentlogin.subscribe(slevel => (this.slevel = slevel));
    console.log(this.slevel);
  }

  getKeyword(keyword) {
    this.isLoading = true;
    this.memberService
      .searchMembers(this.keyword)
      .subscribe((members: Member[]) => {
        this.members = members;
        console.log(members);
        this.recount = members.length;
        if (this.recount === 0) {
          this.alertify.warning('No Records Found!');
        }
        this.isLoading = false;
        this.keyword = '';
      });
  }

  onDelete(master: string) {
    this.alertify.confirm(
      'Are you sure you want to delete this member?',
      () => {
        this.memberService.deleteMember(master).subscribe(
          () => {
            this.alertify.success('Member Deleted');
            this.route.navigateByUrl('/search');
          },
          error => {
            this.alertify.error('Failed to delete...');
          }
        );
      }
    );
  }
}
