import { HttpClientModule } from '@angular/common/http';
import { LeaveOneComponent } from './leave/leave-one/leave-one.component';
import { SubLeaveComponent } from './leave/leaves/sub-leave/sub-leave.component';
import { NewLeaveComponent } from './leave/leaves/new-leave/new-leave.component';
import { LeaveService } from './services/leave.service';
import { LeavesComponent } from './leave/leaves/leaves.component';
import { EditDueComponent } from './due/edit-due/edit-due.component';
import { ViewDuesComponent } from './due/view-dues/view-dues.component';
import { NotesComponent } from './note/notes.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FileUploadModule } from 'ng2-file-upload';

import { AppRoutingModule } from './app.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';

import { OktaAuthModule } from '@okta/okta-angular';
import { NgxPaginationModule } from 'ngx-pagination';
import { TextMaskModule } from 'angular2-text-mask';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { MemberViewComponent } from './member/member-view/member-view.component';
import { MemberEditComponent } from './member/member-edit/member-edit.component';
import { MemberAddComponent } from './member/member-add/member-add.component';
import { AlertifyService } from './services/alertify.service';
import { MemberService } from './services/member.service';
import { EditLeaveComponent } from './leave/edit-leave/edit-leave.component';
import { EditSubLeaveComponent } from './leave/leaves/sub-leave/edit-sub-leave/edit-sub-leave.component';

import {
  FormFieldCustomControlExample,
  MyTelInput
} from '../app/phonemask/form-field-custom-control-example';
import { NoteEditComponent } from './note/note-edit/note-edit.component';
import { NgPipesModule } from 'ngx-pipes';

import {
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatTabsModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule,
  MatRadioModule,
  MatSlideToggleModule
} from '@angular/material';
import { LoginService } from './services/login.service';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { FileComponent } from './file/file.component';
import { ViewCompaniesComponent } from './companys/view-companies/view-companies.component';
import { EditCompanyComponent } from './companys/edit-company/edit-company.component';
import { ViewStatusComponent } from './status/view-status/view-status.component';
import { EditStatusComponent } from './status/edit-status/edit-status.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    SearchComponent,
    MemberViewComponent,
    MemberEditComponent,
    MemberAddComponent,
    FormFieldCustomControlExample,
    MyTelInput,
    NotesComponent,
    NoteEditComponent,
    ViewDuesComponent,
    EditDueComponent,
    LeavesComponent,
    NewLeaveComponent,
    SubLeaveComponent,
    LeaveOneComponent,
    EditLeaveComponent,
    EditSubLeaveComponent,
    FileUploadComponent,
    FileComponent,
    ViewCompaniesComponent,
    EditCompanyComponent,
    ViewStatusComponent,
    EditStatusComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatTabsModule,
    TextMaskModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    FileUploadModule,
    MatSlideToggleModule,
    NgPipesModule,
    HttpClientModule,
    MatSlideToggleModule,
    OktaAuthModule.initAuth({
      issuer: 'https://dev-555668.okta.com/oauth2/ausaz9g21xkkzzBbX356',
      redirectUri: 'http://192.168.1.200:4200/implicit/callback',
      clientId: '0oaazakkviCg1foEm356'
    })
  ],
  providers: [
    AlertifyService,
    MemberService,
    DatePipe,
    LeaveService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
