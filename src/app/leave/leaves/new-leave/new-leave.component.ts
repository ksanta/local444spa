import { NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Workplace } from './../../../models/Workplace';
import { MemberService } from './../../../services/member.service';
import { Leave } from './../../../models/Leave';
import { AlertifyService } from './../../../services/alertify.service';
import { LeaveService } from './../../../services/leave.service';


export interface Type {
  value: string;
  viewValue: string;
}

export interface Typer {
  value: string;
  viewValue: string;
}

export interface Payment {
  id: number;
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-new-leave',
  templateUrl: './new-leave.component.html',
  styleUrls: ['./new-leave.component.css']
})
export class NewLeaveComponent implements OnInit {
  @ViewChild('f') addleave: NgForm;
  workplaces: Workplace[];
  scompany: string;
  leaves: Leave[];
  model: any = {};
  leave: any = {};
  vId: number;
  addM: boolean;
  btnSave = false;
  workplace: Workplace;
  semails: any;
  checked = false;

  payments: Payment[] = [
    { id: 1, value: 'Paid Value', viewValue: 'THIS IS A PAID LEAVE **'},
    { id: 2, value: 'Unpaid Leave', viewValue: 'THIS IS AN UNPAID LEAVE **'}
  ];

  types: Type[] = [
    { value: 'Leave', viewValue: 'Leave' },
    { value: 'Cancellation', viewValue: 'Cancellation' }
  ];

  typers: Typer[] = [
    { value: 'dr-uni240', viewValue: 'dr-uni240' },
    { value: 'tm-uni240', viewValue: 'tm-uni240' },
    { value: 'km-uni240', viewValue: 'km-uni240' }
  ];

  constructor(
    private leaveService: LeaveService,
    private alertify: AlertifyService,
    private memberService: MemberService
  ) {}

  ngOnInit() {
    this.loadCompanys();
  }

  // method triggered when form is submitted
  Submit() {
    console.log(this.model.notes);
    // tslint:disable-next-line: triple-equals
    this.model.emails = this.workplace;
    console.log(this.model.emails);
    this.leaveService.addLeave(this.model).subscribe(
      () => {
        console.log(this.model);
        this.alertify.success('Adding Members');
        this.btnSave = !this.btnSave;
        this.addM = true;
        this.getNewId();
      },
      error => {
        this.alertify.error('Leave phase 1 could not be saved.');
      }
    );
  }

  loadCompanys() {
    this.memberService.getCompanys().subscribe(
      (workplaces: Workplace[]) => {
        this.workplaces = workplaces;
        console.log(this.workplaces);
      },
      error => {
        this.alertify.error('could not pull the list of companies.');
      }
    );
  }

  getNewId() {
    this.btnSave = true;
    this.leaveService.getLastLeave().subscribe(
      (leave: Leave) => {
        this.leave = leave;
        console.log(this.leave);
        this.alertify.success('got new id');
        this.vId = leave.id;
        console.log(this.vId);
        this.addM = true;
      },
      error => {
        this.alertify.error('no go id.');
      }
    );
  }
  getEmails() {
    this.leaveService.getEmails(this.scompany).subscribe(
      (workplace: Workplace) => {
        this.workplace = workplace;
        console.log(this.workplace);
        this.semails = workplace;
      },
      error => {
        this.alertify.error('could not pull the list of company emails.');
      }
    );
  }

  onCompanyChange(value) {
    this.scompany = this.addleave.value.company;
    this.getEmails();
    console.log(this.scompany);
  }
    }
