import { Component, OnInit } from '@angular/core';
import { AlertifyService } from './../../services/alertify.service';
import { Leave } from './../../models/Leave';
import { LeaveService } from './../../services/leave.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-leaves',
  templateUrl: './leaves.component.html',
  styleUrls: ['./leaves.component.css']
})
export class LeavesComponent implements OnInit {
  leaves = [];
  p = 1;

  constructor(
    private leaveService: LeaveService,
    private alertify: AlertifyService,
    private route: Router
  ) {}

  ngOnInit() {
    this.loadLeaves();
  }

  loadLeaves() {
    this.leaveService.getLeaves().subscribe((leaves: Leave[]) => {
      this.leaves = leaves;
      console.log(this.leaves);
    });
  }

  onDelete(id) {
    this.alertify.confirm(
      'Are you sure you want to delete this leave?',
      () => {
        this.leaveService.deleteLeave(id).subscribe(
          () => {
            this.alertify.success('Leave Deleted');
            this.loadLeaves();
          },
          error => {
            this.alertify.error('Failed to delete...');
          }
        );
      }
    );
  }
}
