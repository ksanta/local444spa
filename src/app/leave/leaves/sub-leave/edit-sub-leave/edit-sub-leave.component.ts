import { Component, OnInit, Input } from '@angular/core';
import { Lmember } from '../../../../models/Lmember';
import { Member } from '../../../../models/member';
import { MemberService } from '../../../../services/member.service';
import { AlertifyService } from '../../../../services/alertify.service';
import { LeaveService } from '../../../../services/leave.service';
import { Leave } from '../../../../models/Leave';
import * as _ from 'lodash';

@Component({
  selector: 'app-edit-sub-leave',
  templateUrl: './edit-sub-leave.component.html',
  styleUrls: ['./edit-sub-leave.component.css']
})
export class EditSubLeaveComponent implements OnInit {
  master = '';
  isLoading = false;
  array: Array<Lmember> = [];
  body: any;
  acount: number;
  member: Member;
  @Input() leave: Leave;
  @Input() vNotation: string;
  @Input() vId: string;
  @Input() addM: boolean;
  printButton = false;
  allowEdit = false;

  constructor(
    private memberService: MemberService,
    private alertify: AlertifyService,
    private leaveService: LeaveService,
  ) {}

  ngOnInit() {
    this.array = this.leave.subleaves;
  }

  getKeyword(master) {
    this.isLoading = true;
    this.memberService.getMember(this.master).subscribe(
      (member: Member) => {
        this.member = member;
        console.log(member);
        this.isLoading = false;
        console.log(this.vId);
        this.array.push({
          leave_no: String(this.vId),
          master: this.member.master,
          first: this.member.first,
          last: this.member.last,
          dept: this.member.dept,
          notation: this.vNotation
        }),
          console.log(JSON.stringify(this.array));
      },
      error => {
        this.alertify.error('Master you entered was not found!');
      }
    );
  }

  Submit() {
    JSON.stringify(this.array);
    this.removeDuplicates(this.array);
    console.log(this.array);
    this.leaveService.postMemberArray(this.array).subscribe(
      (response: Response) => {
        this.body = response.body;
        console.log(response.body);
        this.alertify.success('Leave Saved.');
        this.printButton = true;
      },
      error => {
        this.alertify.error('The Leave could not be saved.');
      }
    );
  }

  onRemove(master) {
    this.array.splice(this.array.indexOf(master), 1);
  }

  editSub() {
    console.log(this.vId);
    this.allowEdit = true;
    this.leaveService.deleteSubs(+this.vId).subscribe();
    this.alertify.warning('deleting' + this.vId);
  }

    removeDuplicates(array) {
      const result = _.uniqBy(array, 'master');
      console.log(result)
      this.array = result;
    }
  }

