import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { MemberService } from './../../../services/member.service';
import { AlertifyService } from './../../../services/alertify.service';
import { LeaveService } from './../../../services/leave.service';
import { Member } from './../../../models/Member';
import { Lmember } from './../../../models/Lmember';

@Component({
  selector: 'app-sub-leave',
  templateUrl: './sub-leave.component.html',
  styleUrls: ['./sub-leave.component.css']
})
export class SubLeaveComponent {
  master = '';
  isLoading = false;
  array: Array<Lmember> = [];
  body: any;
  acount: number;
  member: Member;
  @Input() vNotation: string;
  @Input() vId: string;
  @Input() addM: boolean;
  printButton = false;
  saveButton = false;
  editButton = false;
  @ViewChild('master') searchField: ElementRef;


  constructor(
    private memberService: MemberService,
    private alertify: AlertifyService,
    private leaveService: LeaveService
  ) {}

  getKeyword(master) {
    this.isLoading = true;
    this.memberService.getMember(this.master).subscribe(
      (member: Member) => {
        this.member = member;
        console.log(member);
        this.isLoading = false;
        console.log(this.vId);
        this.array.push({
          leave_no: String(this.vId),
          master: this.member.master,
          first: this.member.first,
          last: this.member.last,
          dept: this.member.dept,
          notation: this.vNotation
        });
        const name = this.member.first + '' + this.member.last;
        this.alertify.success('You added: ' + name);
        this.printButton = false;
        this.saveButton = true;
        this.master = '';
        this.focusMaster();
      },
      error => {
        this.alertify.error('Master you entered was not found!');
      }
    );
  }

  Submit() {
    JSON.stringify(this.array);
    this.alertify.confirm('Save Leave? -- Make sure everything is correct!', () => {
      this.leaveService.postMemberArray(this.array).subscribe(
        (response: Response) => {
          this.body = response.body;
          console.log(response.body);
          this.alertify.success('Leave Saved.');
          this.printButton = true;
          this.saveButton = false;
          this.editButton = true;
        },
        error => {
          this.alertify.error('The Leave could not be saved.');
        }
          );
        }
      );
    }

  onRemove(master) {
    this.array.splice(this.array.indexOf(master), 1);
    this.printButton = false;
  }

  focusMaster(): void {
    this.searchField.nativeElement.focus();
}

}
