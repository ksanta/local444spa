import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { LeaveService } from '../../services/leave.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from '../../services/alertify.service';
import { Leave } from '../../models/Leave';
import { NgForm } from '@angular/forms';
import { MemberService } from '../../services/member.service';
import { Workplace } from '../../models/Workplace';

export interface Type {
  value: string;
  viewValue: string;
}

export interface Typer {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-edit-leave',
  templateUrl: './edit-leave.component.html',
  styleUrls: ['./edit-leave.component.css']
})
export class EditLeaveComponent implements OnInit {
  @ViewChild('editleave') form: NgForm;
  leave: Leave;
  workplaces: Workplace[];
  isLoading = false;
  @Input() view: boolean;
  vId;
  types: Type[] = [
    { value: 'Leave', viewValue: 'Leave' },
    { value: 'Cancellation', viewValue: 'Cancellation' }
  ];

  typers: Typer[] = [
    { value: 'dr-uni240', viewValue: 'dr-uni240' },
    { value: 'tm-uni240', viewValue: 'tm-uni240' },
    { value: 'km-uni240', viewValue: 'km-uni240' }
  ];


  constructor(
    private leaveService: LeaveService,
    private route: ActivatedRoute,
    private alertify: AlertifyService,
    private router: Router,
    private memberService: MemberService
  ) {}

  ngOnInit() {
    this.view = false;
    this.loadCompanys();
    this.loadLeave();
  }

  loadCompanys() {
    this.memberService.getCompanys().subscribe(
      (workplaces: Workplace[]) => {
        this.workplaces = workplaces;
        console.log(this.workplaces);
      },
      error => {
        this.alertify.error('could not pull the list of companies.');
      }
    );
  }

  loadLeave() {
    this.view = false;
    this.isLoading = true;
    this.leaveService.getLeave(this.route.snapshot.params['id']).subscribe(
      (leave: Leave) => {
        this.leave = leave;
        console.log(this.leave);
        this.isLoading = false;
        this.alertify.success('leave loaded! Leave No.' +this.leave.id);
        this.vId = this.leave.id;
      },
      error => {
        this.alertify.error('there has been an error');
        this.isLoading = false;
      }
    );
  }
  backToLeaves() {
    this.router.navigateByUrl('/leaves');
  }

  Submit(form: NgForm ) {
    this.leaveService
        .updateLeave(this.route.snapshot.params['id'], this.leave)
        .subscribe(
          next => {
            this.alertify.success('profile updated successfully.');
          },
          error => {
            this.alertify.error('profile could not be saved.');
          }
        );
  }
}
