import { Component, OnInit, Input } from '@angular/core';
import { Leave } from './../../models/Leave';
import { LeaveService } from './../../services/leave.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from '../../services/alertify.service';

@Component({
  selector: 'app-leave-one',
  templateUrl: './leave-one.component.html',
  styleUrls: ['./leave-one.component.css']
})
export class LeaveOneComponent implements OnInit {
  leave: Leave;
  isLoading = false;
  @Input() view: boolean;
  email: any;
  parm = this.route.snapshot.params['id'];

  constructor(
    private leaveService: LeaveService,
    private route: ActivatedRoute,
    private alertify: AlertifyService,
    private router: Router
  ) {}

  ngOnInit() {
    this.view = false;
    this.loadLeave();
  }

  loadLeave() {
    this.view = false;
    this.isLoading = true;
    this.leaveService.getLeave(this.route.snapshot.params['id']).subscribe(
      (leave: Leave) => {
        this.leave = leave;
        console.log(this.leave);
        this.isLoading = false;
        this.alertify.success('leave loaded!');
      },
      error => {
        this.alertify.error('there has been an error');
        this.isLoading = false;
      }
    );
  }
  backToLeaves() {
    this.router.navigateByUrl('/leaves');
  }

  sendEmails() {
    window.location.href='http://192.168.1.200/laravelapi/public/mail/' + this.parm;
}

}