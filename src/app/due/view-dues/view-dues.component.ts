import { Component, OnInit, ViewChild } from '@angular/core';
import { MemberService } from '../../services/member.service';
import { AlertifyService } from '../../services/alertify.service';
import { ActivatedRoute } from '@angular/router';
import { Due } from '../../models/Due';
import { NgForm } from '@angular/forms';
import { LoginService } from './../../services/login.service';

export interface Type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-view-dues',
  templateUrl: './view-dues.component.html',
  styleUrls: ['./view-dues.component.css']
})
export class ViewDuesComponent implements OnInit {
  types: Type[] = [
    { value: 'yearly', viewValue: 'Yearly' },
    { value: 'refund', viewValue: 'Refund' },
    { value: 'owes', viewValue: 'Owes' }
  ];

  dues: Due[];
  parmmaster: any;
  shownote: boolean;
  model: any = {};
  due: Due;
  slevel: number;
  name: string;

  constructor(
    private memberService: MemberService,
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private loginService: LoginService
  ) {}

  ngOnInit() {
    this.loginService.currentlogin.subscribe(slevel => (this.slevel = slevel));
    this.loginService.currentName.subscribe(name => (this.name = name));
    console.log(this.slevel);
    this.loadDues();
    this.parmmaster = this.route.snapshot.params['master'];
  }

  loadDues() {
    this.memberService.getDues(this.route.snapshot.params['master']).subscribe(
      (dues: Due[]) => {
        this.dues = dues;
        console.log(this.dues);
      },
      error => {
        this.alertify.error('did not work');
      }
    );
  }

  showform() {
    this.shownote = !this.shownote;
  }

  Submit() {
    this.model.master_id = this.parmmaster;
    this.memberService.addDues(this.model).subscribe(
      () => {
        console.log(this.model);
        this.alertify.success('Due has been added.');
        location.reload();
      },
      error => {
        this.alertify.error('The Due could not be saved.');
      }
    );
  }

  onDelete(id: string) {
    this.alertify.confirm(
      'Are you sure you want to delete this dues line?',
      () => {
        this.memberService.deleteDue(id).subscribe(
          () => {
            this.alertify.success('Dues line Deleted');
            this.loadDues();
          },
          error => {
            this.alertify.error('Failed to delete...');
          }
        );
      }
    );
  }
}
