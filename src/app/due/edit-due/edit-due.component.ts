import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from './../../services/alertify.service';
import { MemberService } from './../../services/member.service';
import { Due } from '../../models/Due';
import { NgForm, FormControl } from '@angular/forms';

export interface Type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-edit-due',
  templateUrl: './edit-due.component.html',
  styleUrls: ['./edit-due.component.css']
})
export class EditDueComponent implements OnInit {
  @ViewChild('f') editdue: NgForm;
  due: Due;
  isLoading = false;
  masterload: string;
  startd;
  endd;

  types: Type[] = [
    { value: 'yearly', viewValue: 'Yearly' },
    { value: 'refund', viewValue: 'Refund' },
    { value: 'owes', viewValue: 'Owes' }
  ];

  constructor(
    private memberService: MemberService,
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.loadDue();
  }

  loadDue() {
    this.isLoading = true;
    this.memberService.getOneDue(this.route.snapshot.params['id']).subscribe(
      (due: Due) => {
        this.due = due;
        this.masterload = due.master_id;
        if (this.due.start_date) {
          this.startd = new FormControl(new Date(this.due.start_date));
        } else {
          this.startd = new Date(null);
        }
        if (this.due.end_date) {
          this.endd = new FormControl(new Date(this.due.end_date));
        } else {
          this.endd = new Date(null);
        }
        this.isLoading = false;
        this.alertify.success('due loaded!');
      },
      error => {
        this.alertify.error('there has been an error');
        this.isLoading = false;
      }
    );
  }

  updateDue() {
    console.log('test');
    this.editdue.form.patchValue({
      start_date: new Date(this.startd.value),
      end_date: new Date(this.endd.value)
    });
    this.memberService
      .updateDue(this.route.snapshot.params['id'], this.due)
      .subscribe(
        next => {
          this.alertify.success('profile updated successfully.');
          this.router.navigateByUrl('/member-view/' + this.masterload);
        },
        error => {
          this.alertify.error('profile could not be saved.');
        }
      );
  }
}
