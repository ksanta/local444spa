import { Injectable } from '@angular/core';
import { Workplace } from './../models/Workplace';
import { Member } from './../models/Member';
import { Headers, Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Leave } from './../models/Leave';
import { Lmember } from '../models/Lmember';

@Injectable({
  providedIn: 'root'
})
export class LeaveService {
  constructor(private http: Http) {}

  getLeaves(): Observable<Leave[]> {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/leave')
      .pipe(map(response => <Leave[]>response.json()));
  }

  addLeave(model: any) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(
      'http://192.168.1.200/laravelapi/public/api/leave' + '',
      model,
      { headers: headers }
    );
  }
  getLastLeave(): Observable<Leave> {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/leave/last')
      .pipe(map(response => <Leave>response.json()));
  }

  postMemberArray(array) {
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    return this.http
      .post(
        'http://192.168.1.200/laravelapi/public/api/subleave',
        JSON.stringify(array),
        { headers: headers }
      )
      .pipe(map(Response => Response.json()));
  }

  getLeave(id): Observable<Leave> {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/' + 'leave/' + id)
      .pipe(map(response => <Leave>response.json()));
  }

  updateLeave(id: number, leave: Leave) {
    return this.http.put(
      'http://192.168.1.200/laravelapi/public/api/' + 'leave/' + id,
      leave
    );
  }

  deleteSubs(leave_no: number) {
    return this.http.delete(
      'http://192.168.1.200/laravelapi/public/api/' + 'subleave/' + leave_no
    );
  }

  getEmails(company) {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/workplace/' + company)
      .pipe(map(response => response.json()));
  }

  sendEmails(id) {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/mail/' + id)
      .pipe(map(response => response.json()));
  }

  deleteLeave(id) {
    return this.http.delete(
      'http://192.168.1.200/laravelapi/public/api/' + 'leave/' + id
    );
  }

}
