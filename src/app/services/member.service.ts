import { Injectable } from '@angular/core';
import { Workplace } from './../models/Workplace';
import { Funeralhome } from './../models/Funeralhome';
import { Ustatus } from './../models/Ustatus';
import { Member } from './../models/Member';
import { Headers, Http, URLSearchParams } from '@angular/http';
import { Observable, observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Note } from '../models/Note';
import { Due } from '../models/Due';
import { Userfile } from '../models/Userfile';

//http://192.168.1.200/laravelapi/public/api/members

@Injectable({
  providedIn: 'root'
})
export class MemberService {
  baseUrl = 'http://192.168.1.200/laravelapi/public/api/';
  home = 'http://192.168.1.200:8888/laravel/laravelapi/public/api/search';
  constructor(private http: Http) {}

  searchMembers(keyword): Observable<Member[]> {
    let params: URLSearchParams = new URLSearchParams();
    console.log(keyword);
    params.set('keyword', keyword);
    const url = 'http://192.168.1.200/laravelapi/public/api/search';
    return this.http
      .get(url, {
        search: params
      })
      .pipe(map(response => <Member[]>response.json()));
  }

  getMember(master): Observable<Member> {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/' + 'members/' + master)
      .pipe(map(response => <Member>response.json()));
  }

  getCompanys(): Observable<Workplace[]> {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/workplace')
      .pipe(map(response => <Workplace[]>response.json()));
  }

  getFhomes(): Observable<Funeralhome[]> {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/fhomes')
      .pipe(map(response => <Funeralhome[]>response.json()));
  }

  getStatus(): Observable<Ustatus[]> {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/ustatu')
      .pipe(map(response => <Ustatus[]>response.json()));
  }

  updateMember(master: string, member: Member) {
    return this.http.put(
      'http://192.168.1.200/laravelapi/public/api/' + 'members/' + master,
      member
    );
  }
  addMember(model: any) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(
      'http://192.168.1.200/laravelapi/public/api/members' + '',
      model,
      { headers: headers }
    );
  }

  getNotes(master): Observable<Note[]> {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/notes/' + master)
      .pipe(map(response => <Note[]>response.json()));
  }

  getOneNote(id) {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/note/' + id)
      .pipe(map(response => <Note>response.json()));
  }

  addNote(model: any) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(
      'http://192.168.1.200/laravelapi/public/api/notes' + '',
      model,
      { headers: headers }
    );
  }

  addCompany(model: any) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(
      'http://192.168.1.200/laravelapi/public/api/workplace' + '',
      model,
      { headers: headers }
    );
  }

  updateNote(id: number, note: Note) {
    return this.http.put(
      'http://192.168.1.200/laravelapi/public/api/' + 'notes/' + id,
      note
    );
  }

  getDues(master): Observable<Due[]> {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/dues/' + master)
      .pipe(map(response => <Due[]>response.json()));
  }

  addDues(model: any) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(
      'http://192.168.1.200/laravelapi/public/api/dues' + '',
      model,
      { headers: headers }
    );
  }

  getOneDue(id) {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/due/' + id)
      .pipe(map(response => <Due>response.json()));
  }

  updateDue(id: number, due: Due) {
    return this.http.put(
      'http://192.168.1.200/laravelapi/public/api/' + 'dues/' + id,
      due
    );
  }

  deleteMember(master: string) {
    return this.http.delete(
      'http://192.168.1.200/laravelapi/public/api/' + 'members/' + master
    );
  }

  getFiles(master): Observable<Userfile[]> {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/files/' + master)
      .pipe(map(response => <Userfile[]>response.json()));
  }

  deleteNote(id: string) {
    return this.http.delete(
      'http://192.168.1.200/laravelapi/public/api/' + 'notes/' + id
    );
  }

  deleteDue(id: string) {
    return this.http.delete(
      'http://192.168.1.200/laravelapi/public/api/' + 'dues/' + id
    );
  }

  deleteFile(id: string) {
    return this.http.delete(
      'http://192.168.1.200/laravelapi/public/api/' + 'files/' + id
    );
  }

  getOneCompany(id) {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/workplace/1/' + id)
      .pipe(map(response => <Workplace>response.json()));
  }

  updateCompany(id: number, company: Workplace) {
    return this.http.put(
      'http://192.168.1.200/laravelapi/public/api/' + 'workplace/' + id,
      company
    );
  }

  deleteCompany(id: string) {
    return this.http.delete(
      'http://192.168.1.200/laravelapi/public/api/' + 'workplace/' + id
    );
  }
  addStatus(model: any) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(
      'http://192.168.1.200/laravelapi/public/api/status' + '',
      model,
      { headers: headers }
    );
  }

  getOneStatus(id) {
    return this.http
      .get('http://192.168.1.200/laravelapi/public/api/status/1/' + id)
      .pipe(map(response => <Ustatus>response.json()));
  }

  updateStatus(id: number, status: Ustatus) {
    return this.http.put(
      'http://192.168.1.200/laravelapi/public/api/' + 'status/' + id,
      status
    );
  }

  deleteStatus(id: string) {
    return this.http.delete(
      'http://192.168.1.200/laravelapi/public/api/' + 'status/' + id
    );
  }

}
