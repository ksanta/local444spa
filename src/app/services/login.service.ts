import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class LoginService {
  private loginSource = new BehaviorSubject(0);
  currentlogin = this.loginSource.asObservable();

  private loginName = new BehaviorSubject('');
  currentName = this.loginName.asObservable();

  constructor() {}

  changeMessage(slevel: number) {
    this.loginSource.next(slevel);
  }

  changeName(name: string) {
    this.loginName.next(name);
  }
}
