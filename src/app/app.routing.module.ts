import { FileUploadComponent } from './file-upload/file-upload.component';
import { EditCompanyComponent} from './companys/edit-company/edit-company.component';
import { LeaveOneComponent } from './leave/leave-one/leave-one.component';
import { Routes, RouterModule } from '@angular/router';
import { NewLeaveComponent } from './leave/leaves/new-leave/new-leave.component';
import { LeavesComponent } from './leave/leaves/leaves.component';
import { EditDueComponent } from './due/edit-due/edit-due.component';
import { NoteEditComponent } from './note/note-edit/note-edit.component';
import { MemberAddComponent } from './member/member-add/member-add.component';
import { MemberEditComponent } from './member/member-edit/member-edit.component';
import { MemberViewComponent } from './member/member-view/member-view.component';
import { EditLeaveComponent } from './leave/edit-leave/edit-leave.component';
import { NgModule } from '@angular/core';

import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { OktaCallbackComponent, OktaAuthGuard } from '@okta/okta-angular';
import { ViewCompaniesComponent } from './companys/view-companies/view-companies.component';
import { ViewStatusComponent } from './status/view-status/view-status.component';
import { EditStatusComponent } from './status/edit-status/edit-status.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'search', component: SearchComponent }, //canActivate: [OktaAuthGuard] },
  { path: 'implicit/callback', component: OktaCallbackComponent },
  { path: 'member-view/:master', component: MemberViewComponent },
  { path: 'member-edit/:master', component: MemberEditComponent },
  { path: 'note-edit/:id', component: NoteEditComponent },
  { path: 'edit-due/:id', component: EditDueComponent },
  { path: 'member-add', component: MemberAddComponent },
  { path: 'leaves', component: LeavesComponent },
  { path: 'new-leave', component: NewLeaveComponent },
  { path: 'leave-one/:id', component: LeaveOneComponent },
  { path: 'edit-leave/:id', component: EditLeaveComponent },
  { path: 'file-upload', component: FileUploadComponent },
  { path: 'view-companies', component: ViewCompaniesComponent},
  { path: 'edit-company/:id', component: EditCompanyComponent},
  { path: 'view-status', component: ViewStatusComponent},
  { path: 'edit-status/:id', component: EditStatusComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
